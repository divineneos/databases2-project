﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Baze2_Projekat
{
    public partial class DodajCovjekaFrm : Form
    {
        SqlConnection connection;
        public DodajCovjekaFrm(SqlConnection connection)
        {
            InitializeComponent();
            this.connection = connection;
        }

        private void ucitajGradove()
        {
            String query = "SELECT * FROM Grad";
            new FillComboBoxWithPairs(query, connection, comboBox1, "naziv", "naziv").fill();
        }

        private void ucitajStrucneSpreme()
        {
            String query = "SELECT * FROM StrucnaSprema";
            new FillComboBoxWithPairs(query, connection, comboBox2, "stepen", "naziv").fill();
        }

        private void dodajCovjeka()
        {
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == ""
                || comboBox1.SelectedIndex == -1 || comboBox2.SelectedIndex == -1)
            {
                MessageBox.Show("Unesite sve trazene vrijednosti");
                return;
            }

            String query = "INSERT INTO Covjek VALUES(@jmbg, @ime, @prezime, @tel, @grad, @sss)";
            SqlCommand command = this.connection.CreateCommand();
            command.Parameters.Add("jmbg", SqlDbType.VarChar);
            command.Parameters.Add("ime", SqlDbType.VarChar);
            command.Parameters.Add("prezime", SqlDbType.VarChar);
            command.Parameters.Add("tel", SqlDbType.VarChar);
            command.Parameters.Add("grad", SqlDbType.VarChar);
            command.Parameters.Add("sss", SqlDbType.VarChar);
            command.Parameters["jmbg"].Value = textBox1.Text;
            command.Parameters["ime"].Value = textBox2.Text;
            command.Parameters["prezime"].Value = textBox3.Text;
            command.Parameters["tel"].Value = textBox4.Text;
            command.Parameters["grad"].Value = (comboBox1.SelectedItem as ComboboxItem).Key;
            command.Parameters["sss"].Value = (comboBox2.SelectedItem as ComboboxItem).Key;
            command.CommandText = query;

            try
            {
                command.ExecuteNonQuery();
                MessageBox.Show("Uspjesno dodata osoba!");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Nije dodata osoba: " + ex.Message);
            }
        }

        private void DodajCovjekaFrm_Load(object sender, EventArgs e)
        {
            ucitajGradove();
            ucitajStrucneSpreme();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dodajCovjeka();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
