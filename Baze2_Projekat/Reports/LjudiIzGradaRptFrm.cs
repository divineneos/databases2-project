﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Baze2_Projekat.Reports
{
    public partial class LjudiIzGradaRptFrm : Form
    {
        string grad;
        public LjudiIzGradaRptFrm(string grad)
        {
            InitializeComponent();
            this.grad = grad;
        }

        private void LjudiIzGradaRptFrm_Load(object sender, EventArgs e)
        {
            LjudiIzGradaRpt report = new LjudiIzGradaRpt();
            report.SetParameterValue("grad", this.grad);
            this.crystalReportViewer1.ReportSource = report;
        }
    }
}
