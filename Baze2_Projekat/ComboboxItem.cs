﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baze2_Projekat
{
    public class ComboboxItem
    {
        public string Value { get; set; }
        public string Key { get; set; }

        public ComboboxItem(string Key, string Value)
        {
            this.Key = Key;
            this.Value = Value;
        }

        public override string ToString()
        {
            return Value;
        }
    }
}
