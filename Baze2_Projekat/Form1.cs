﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Baze2_Projekat.Reports;

namespace Baze2_Projekat
{
    public partial class Form1 : Form
    {
        DatabaseConnection dbConnection;
        SqlConnection connection;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DodajGradFrm forma = new DodajGradFrm(this.connection);
            forma.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dbConnection = new DatabaseConnection();
            connection = dbConnection.ConnectToDatabase();
            if(connection == null)
                this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DodajCovjekaFrm forma = new DodajCovjekaFrm(this.connection);
            forma.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DodajStrucnuSpremuFrm forma = new DodajStrucnuSpremuFrm(this.connection);
            forma.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            PrikaziGradoveFrm forma = new PrikaziGradoveFrm(this.connection);
            forma.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            SpisakGradovaRptFrm report = new SpisakGradovaRptFrm();
            report.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            IzaberiGradFrm forma = new IzaberiGradFrm(this.connection);
            forma.Show();
        }
    }
}
