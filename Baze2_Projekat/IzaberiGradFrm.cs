﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Baze2_Projekat.Reports;

namespace Baze2_Projekat
{
    public partial class IzaberiGradFrm : Form
    {
        SqlConnection connection;
        public IzaberiGradFrm(SqlConnection connection)
        {
            InitializeComponent();
            this.connection = connection;
        }

        private void ucitajGradove()
        {
            String query = "SELECT * FROM Grad";
            new FillComboBoxWithPairs(query, connection, comboBox1, "naziv", "naziv").fill();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LjudiIzGradaRptFrm reportFrm = new LjudiIzGradaRptFrm((comboBox1.SelectedItem as ComboboxItem).Key);
            reportFrm.Show();
        }

        private void IzaberiGradFrm_Load(object sender, EventArgs e)
        {
            ucitajGradove();
        }
    }
}
