﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Baze2_Projekat
{
    class DatabaseConnection
    {
        String connectionString = @"Server=localhost\SQLEXPRESS;Database=Baze2;Integrated Security=True";
        SqlConnection connection;

        public SqlConnection ConnectToDatabase()
        {
            try
            {
                connection = new SqlConnection();
                connection.ConnectionString = this.connectionString;
                connection.Open();
                return connection;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Greska pri konektovanju na bazu: " + ex.Message);
                return null;
            }
        }
    }
}
