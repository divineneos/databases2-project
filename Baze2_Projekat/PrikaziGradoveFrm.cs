﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Baze2_Projekat
{
    public partial class PrikaziGradoveFrm : Form
    {
        SqlConnection connection;
        DataTable dt;

        public PrikaziGradoveFrm(SqlConnection connection)
        {
            InitializeComponent();
            this.connection = connection;
        }

        private void popuniTabeluGradova()
        {
            dt = new DataTable();

            SqlCommand command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM Grad";

            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;

            adapter.Fill(dt);

            dataGridView1.DataSource = dt;
        }

        private void PrikaziGradoveFrm_Load(object sender, EventArgs e)
        {
            popuniTabeluGradova();
        }

        private void dodajNoviToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DodajGradFrm forma = new DodajGradFrm(this.connection);
            DialogResult result = forma.ShowDialog();
            // Refresh
            if(result == DialogResult.OK)
            {
                popuniTabeluGradova();
            }
        }

        private void izmijeniToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Izaberite grad za izmjenu!");
                return;
            }

            DodajGradFrm forma = new DodajGradFrm(this.connection, dataGridView1.SelectedRows[0].Cells["naziv"]);
            DialogResult result = forma.ShowDialog();
            popuniTabeluGradova();
        }

        private void obrisiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Izaberite grad za brisanje!");
                return;
            }

            string query = "DELETE FROM Grad WHERE Naziv = @naziv";
            SqlCommand command = connection.CreateCommand();
            command.Parameters.Add("naziv", SqlDbType.VarChar);
            command.Parameters["naziv"].Value = dataGridView1.SelectedRows[0].Cells["naziv"].Value.ToString();
            command.CommandText = query;

            try
            {
                command.ExecuteNonQuery();
                MessageBox.Show("Uspjesno obrisan grad!");
                popuniTabeluGradova();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Neuspjesno obrisan grad: " + ex.Message);
            }
        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
