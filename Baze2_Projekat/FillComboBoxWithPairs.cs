﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Baze2_Projekat
{
    class FillComboBoxWithPairs
    {
        string query;
        SqlConnection connection;
        ComboBox combo;
        string keyColumn;
        string valueColumn;

        public FillComboBoxWithPairs(string query, SqlConnection connection, ComboBox combo, string keyColumn, string valueColumn)
        {
            this.query = query;
            this.connection = connection;
            this.combo = combo;
            this.keyColumn = keyColumn;
            this.valueColumn = valueColumn;
        }

        public void fill()
        {
            SqlCommand command = connection.CreateCommand();
            command.CommandText = query;

            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                //Ovim pokazati kako se cita sve i prikazati procitano
                //MessageBox.Show(reader["naziv"].ToString());
                ComboboxItem newCbItem = new ComboboxItem(reader[keyColumn].ToString(), reader[valueColumn].ToString());
                combo.Items.Add(newCbItem);
            }

            //OBAVEZNO!!!
            reader.Close();

            //Da ne bi mogao da se mijenja sadrzaj comboboxa
            combo.DropDownStyle = ComboBoxStyle.DropDownList;
        }
    }
}
