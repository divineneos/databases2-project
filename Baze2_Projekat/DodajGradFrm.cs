﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Baze2_Projekat
{
    public partial class DodajGradFrm : Form
    {
        SqlConnection connection;
        private string primaryKey = "";

        // Predajemo konekciju u konstruktoru
        public DodajGradFrm(SqlConnection connection)
        {
            InitializeComponent();
            this.connection = connection;
        }

        // Konstruktor koji koristimo za izmjenu - sem konekcije, predajemo i celiju u kojoj se nalazi kljuc
        public DodajGradFrm(SqlConnection connection, DataGridViewCell cell) : this(connection)
        {
            this.primaryKey = cell.Value.ToString();
            ucitajGrad();
        }

        private void dodajGrad()
        {
            if(textBox1.Text == "" || textBox2.Text == "")
            {
                MessageBox.Show("Unesite sve trazene vrijednosti");
                return;
            }

            int populacija;
            bool jeBroj = Int32.TryParse(textBox2.Text, out populacija);
            if(!jeBroj)
            {
                MessageBox.Show("Druga vrijednost nije cijeli broj!");
                return;
            }

            String query = "INSERT INTO Grad VALUES(@naziv, @populacija)";

            SqlCommand command = this.connection.CreateCommand();
            command.Parameters.Add("naziv", SqlDbType.VarChar);
            command.Parameters.Add("populacija", SqlDbType.Int);
            command.Parameters["naziv"].Value = textBox1.Text;
            command.Parameters["populacija"].Value = textBox2.Text;
            command.CommandText = query;

            try
            {
                command.ExecuteNonQuery();
                MessageBox.Show("Uspjesno dodat grad!");
                this.DialogResult = DialogResult.OK; // Ako se forma prikaze u vidu dialoga, moze da se vrati status
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Nije dodat grad: " + ex.Message);
            }
        }

        private void izmijeniGrad()
        {
            if (textBox1.Text == "" || textBox2.Text == "")
            {
                MessageBox.Show("Unesite sve trazene vrijednosti");
                return;
            }

            int populacija;
            bool jeBroj = Int32.TryParse(textBox2.Text, out populacija);
            if (!jeBroj)
            {
                MessageBox.Show("Druga vrijednost nije cijeli broj!");
                return;
            }

            String query = "UPDATE Grad SET Naziv=@naziv, Populacija=@populacija WHERE Naziv=@starinaziv";

            SqlCommand command = this.connection.CreateCommand();
            command.Parameters.Add("naziv", SqlDbType.VarChar);
            command.Parameters.Add("populacija", SqlDbType.Int);
            command.Parameters.Add("starinaziv", SqlDbType.VarChar);
            command.Parameters["naziv"].Value = textBox1.Text;
            command.Parameters["populacija"].Value = textBox2.Text;
            command.Parameters["starinaziv"].Value = primaryKey;
            command.CommandText = query;

            try
            {
                command.ExecuteNonQuery();
                MessageBox.Show("Uspjesno azuriran grad!");
                this.DialogResult = DialogResult.OK; // Ako se forma prikaze u vidu dialoga, moze da se vrati status
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Nije azuriran grad: " + ex.Message);
            }
        }

        private void ucitajGrad()
        {
            SqlCommand command = connection.CreateCommand();
            command.Parameters.Add("naziv", SqlDbType.VarChar);
            command.Parameters["naziv"].Value = primaryKey;
            command.CommandText = "SELECT * FROM Grad WHERE Naziv=@naziv";

            SqlDataReader reader = command.ExecuteReader();
            reader.Read();

            textBox1.Text = reader["naziv"].ToString();
            textBox2.Text = reader["populacija"].ToString();

            reader.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (primaryKey == "") dodajGrad();
            else izmijeniGrad();
        }
    }
}
