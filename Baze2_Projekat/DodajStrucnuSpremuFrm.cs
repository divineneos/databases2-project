﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Baze2_Projekat
{
    public partial class DodajStrucnuSpremuFrm : Form
    {
        SqlConnection connection;

        public DodajStrucnuSpremuFrm(SqlConnection connection)
        {
            InitializeComponent();
            this.connection = connection;
        }

        private void dodajSSS()
        {
            if (textBox1.Text == "" || textBox2.Text == "")
            {
                MessageBox.Show("Unesite sve trazene vrijednosti");
                return;
            }

            int stepen;
            bool jeBroj = Int32.TryParse(textBox1.Text, out stepen);
            if (!jeBroj)
            {
                MessageBox.Show("Prva vrijednost nije cijeli broj!");
                return;
            }

            String query = "INSERT INTO StrucnaSprema VALUES(@stepen, @naziv)";

            SqlCommand command = this.connection.CreateCommand();
            command.Parameters.Add("stepen", SqlDbType.Int);
            command.Parameters.Add("naziv", SqlDbType.VarChar);
            command.Parameters["stepen"].Value = textBox1.Text;
            command.Parameters["naziv"].Value = textBox2.Text;
            command.CommandText = query;

            try
            {
                command.ExecuteNonQuery();
                MessageBox.Show("Uspjesno dodata strucna sprema!");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Nije dodata strucna sprema: " + ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dodajSSS();
        }
    }
}
